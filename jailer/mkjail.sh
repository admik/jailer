#!/bin/sh

# check arguments
if [ $# -ne 2 ]
then
	echo "Usage: mkjail path name";
exit 1;
fi

path=$1
name=$2

if [ -d ${path} ]
then
        echo ">> directory is found: '${path}'"
else
	echo ">> directory is not found (will be created): '${path}'"
fi

echo -n "continue? (yes/no) [yes]: "
read yes_no;

case "$yes_no" in
	"yes")
	;;
	"")
	;;
	*)
		exit 0;
	;;
esac

if [ -d ${path} ]
then
else
	echo ">> creating '${path}'...";
	mkdir ${path} || exit 1;
fi

echo ">> creating base system directories hier...";

mtree -deU -f /etc/mtree/BSD.root.dist -p ${path}/ > /dev/null || exit 1;
mtree -deU -f /etc/mtree/BSD.usr.dist -p ${path}/usr/ > /dev/null || exit 1;
mtree -deU -f /etc/mtree/BSD.include.dist -p ${path}/usr/include/ > /dev/null || exit 1;
mtree -deU -f /etc/mtree/BSD.local.dist -p ${path}/usr/local/ > /dev/null || exit 1;
mtree -deU -f /etc/mtree/BSD.var.dist -p ${path}/var/ > /dev/null || exit 1;
mtree -deU -f /etc/mtree/BIND.chroot.dist -p ${path}/var/named/ > /dev/null || exit 1;
mtree -deU -f /etc/mtree/BSD.sendmail.dist -p ${path}/ > /dev/null || exit 1;

echo ">> pupulating /etc..."
mergemaster -iD ${path}/ || exit 1;

/usr/bin/cap_mkdb ${path}/etc/login.conf
/usr/sbin/pwd_mkdb -d ${path}/etc -p ${path}/etc/master.passwd

cp /etc/localtime $path/etc/
mkdir ${path}/packages

cat <<EOF >>${path}/etc/fstab
/bin			${path}/bin		nullfs	ro	0	0
/lib			${path}/lib		nullfs	ro	0	0
/libexec		${path}/libexec		nullfs	ro	0	0
/sbin			${path}/sbin		nullfs	ro	0	0
/usr/bin		${path}/usr/bin		nullfs	ro	0	0
/usr/include		${path}/usr/include	nullfs	ro	0	0
/usr/lib		${path}/usr/lib		nullfs	ro	0	0
/usr/libdata		${path}/usr/libdata	nullfs	ro	0	0
/usr/libexec		${path}/usr/libexec	nullfs	ro	0	0
/usr/sbin		${path}/usr/sbin	nullfs	ro	0	0
/usr/share		${path}/usr/share	nullfs	ro	0	0

EOF

cat <<EOF >>/etc/rc.conf
jail_${name}_hostname="${name}"
jail_${name}_ip="127.0.0.1"
jail_${name}_interface="lo0"
jail_${name}_rootdir="${path}"
jail_${name}_exec="/bin/sh /etc/rc"
jail_${name}_devfs_enable="YES"
jail_${name}_mount_enable="YES"
jail_${name}_fstab="${path}/etc/fstab"

EOF

cat <<EOF >>$path/etc/rc.conf
hostname=${name}

syslogd_enable="YES"
syslogd_flags="-ss"
sendmail_enable="NONE"

EOF
